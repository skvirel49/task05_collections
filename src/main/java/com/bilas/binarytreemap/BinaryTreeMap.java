package com.bilas.binarytreemap;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BinaryTreeMap<K, V> implements Map<K, V> {

    private Node node;
    private Set<K> keysSet;
    private List<V> valuesSet;
    private static int size = 0;

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public V get(Object key) {
        return null;
    }

    public V put(K key, V value) {
        return null;
    }

    public V remove(Object key) {
        return null;
    }

    public void clear() {

    }

    public boolean containsKey(Object key) {
        return false;
    }

    public boolean containsValue(Object value) {
        return false;
    }

    public void putAll(Map<? extends K, ? extends V> m) {

    }

    public Set<K> keySet() {
        return null;
    }

    public Collection<V> values() {
        return null;
    }

    public Set<Entry<K, V>> entrySet() {
        return null;
    }

    private class Node<K, V> {
        K key;
        V value;
        Node<K,V> leftNode;
        Node<K,V> rightNode;
        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public String toString() {
            return "{" +
                    "key=" + key +
                    ", value='" + value + "'} ";
        }

        K getKey() {
            return key;
        }

        void setKey(final K key) {
            this.key = key;
        }

        V getValue() {
            return value;
        }

        void setValue(final V value) {
            this.value = value;
        }

        Node<K, V> getLeftNode() {
            return leftNode;
        }

        void setLeftNode(final Node<K, V> leftNode) {
            this.leftNode = leftNode;
        }

        Node<K, V> getRightNode() {
            return rightNode;
        }

        void setRightNode(final Node<K, V> rightNode) {
            this.rightNode = rightNode;
        }
    }
}
