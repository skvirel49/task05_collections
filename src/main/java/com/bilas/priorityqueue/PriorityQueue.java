package com.bilas.priorityqueue;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

public class PriorityQueue<E> {

    private static final int DEFAULT_DEQUEUE_SIZE = 15;
    private Object[] objects = new Object[DEFAULT_DEQUEUE_SIZE];
    private int size = 0;
    private final Comparator<? super E> comparator;

    public PriorityQueue(Comparator<? super E> comparator) {
        this.comparator = comparator;
        this.objects = new Object[DEFAULT_DEQUEUE_SIZE];
    }

    public PriorityQueue(int initialCapacity,
                         Comparator<? super E> comparator) {
        if (initialCapacity < 1)
            throw new IllegalArgumentException();
        this.objects = new Object[initialCapacity];
        this.comparator = comparator;
    }

    public PriorityQueue(int initialCapacity) {
        this(initialCapacity, null);
    }

    public PriorityQueue() {
        this.comparator = new Comparator<E>() {
            @Override
            public int compare(E o1, E o2) {
                if (o1.equals(o2)){
                    return 0;
                }
                return -1;
            }

        };
    }


    public int size() {
        return size;
    }

    public boolean isEmpty() {
        int count = 0;
        for (Object o : objects) {
            if (o != null){
                count++;
            }
        }
        return count == 0;
    }

    public void add(E droid) {
        if (droid == null) {
            throw new NullPointerException();
        }
        if(size == objects.length-1)
            changeElementsSize(objects.length*2);
        objects[size++] = droid;
    }



    private void changeElementsSize(int newLength){
        Object[] newArray = new Object[newLength];
        System.arraycopy(objects, 0, newArray, 0, size);
        objects = newArray;
    }

    public boolean remove(Object o) {
        for (int i = 0; i < objects.length; i++) {
            if (o.equals(objects[i])){
                objects[i] = null;
                return true;
            }
        }
        return false;
    }

    public void clear() {
        for (int i = 0; i < size; i++)
            objects[i] = null;
        size = 0;
    }

    public Object poll() {
        if (size == 0) {
            return null;
        }
        int s = --size;
        E result = (E) objects[0];
        E x = (E) objects[s];
        objects[s] = null;
        return result;
    }

    public Object peek() {
        if(size == 0) {
            return null;
        } else {
            return (E) objects[0];
        }
    }

    public boolean contains(Object o) {
        boolean boo = false;
        for (Object ob : objects) {
            if (o.equals(ob)){
                boo = true;
            }
        }
        return boo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriorityQueue<?> that = (PriorityQueue<?>) o;
        return size == that.size &&
                Arrays.equals(objects, that.objects) &&
                Objects.equals(comparator, that.comparator);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(size, comparator);
        result = 31 * result + Arrays.hashCode(objects);
        return result;
    }

    @Override
    public String toString() {
        return "PriorityQueue{" +
                "droids=" + Arrays.toString(objects) +
                ", size=" + size +
                ", comparator=" + comparator +
                '}';
    }
}
